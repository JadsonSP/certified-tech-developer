// 1. Selecione o formulário
let formulario = document.getElementById('formularioDeCadastro')
let listaDados = document.getElementById('listaItems')

// 2. Crie da rotina que será disparada a partir do evento de envio
function createLiElement(conteudo) {
  let liElement = document.createElement('li')
  liElement.classList.add('c-lista__item')

  let contentLi = document.createTextNode(conteudo)
  liElement.appendChild(contentLi)

  listaDados.appendChild(liElement)
}

formulario.addEventListener('submit', e => {
  e.preventDefault()

  let textoNaoTratado = e.target['item'].value
  let textoTratado = textoNaoTratado.replace(' ', '')
  textoTratado = textoTratado.replace(/\d/g, '')
  createLiElement(textoTratado)

  e.target['item'].value = ''
})

// 2.2. Remova todos os espaços vazios

// 2.1. Remova todos os números permitindo apenas texto

// 3. Capture o evento de envio do formulário e disparar a rotina
