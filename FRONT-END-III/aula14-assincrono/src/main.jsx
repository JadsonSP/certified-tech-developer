import React from 'react'
import ReactDOM from 'react-dom/client'
import  EffectExample  from './EffectExample'


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <EffectExample />
  </React.StrictMode>
)
