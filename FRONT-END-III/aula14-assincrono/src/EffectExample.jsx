import { useState, useEffect } from 'react'

function EffectExample(){
  //useState
  const [vetor, setVetor] = useState([]);

  const getData = async () => {
    const data = await
    fetch('https://jsonplaceholder.typicode.com/comments')  // O uso do fetch, que irá interceptar a requisição, levando os dados da API de forma assíncrona e em formato de texto

  const convert = await data.json(); // o método .json() para converter os dados recebidos na variável date de texto para JSON. 

  // Esse é o conceito de assincronia, utilizando o async/await, o qual possibilita que o estado receba os dados da API assim que ela tiver devolvido essas informações, evitando assim que a aplicação entre em colapso.

  setVetor(convert)

  //useEffect

  // O useEffect é usado para trabalhar com o ciclo de vida do componente, e é ideal para trabalhar com retornos da API
  useEffect(() => {
    getData();
  });

  return (
    //enviar esses dados para o navegador de forma organizada.Uma forma de fazê-lo é utilizando .map() para percorrer cada elemento do nosso estado, que será o vetor. 
    <div>
      <ul>
        {vetor.map(object => (<li>{object.email}</li>))}
      </ul> 
    </div>
  )
  }
}

export default EffectExample;

