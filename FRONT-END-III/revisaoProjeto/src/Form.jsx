import React from 'react'

function Form (props){

  function adicionarAnime() {
    if(props.nomeAnime == "" || props.imagemAnime == ""){
      alert("Preencha os campos Nome Anime")
      // styles={color:props.backgroundColor}
    }else{
      props.setAnimes([...props.animes,{
      nome:props.nomeAnime,
      url:props.imagemAnime
    }])
    }
  }
  <form>
      <SearchInput value={props.text} onChange={(search) => props.setText(search)}/>
      <input value={props.nomeAnime} onChange={(event) => props.setNomeAnime(event.target.value)} placeholder="Digite o nome do anime"/>
     
      <input value={props.imagemAnime} onChange={(event) => props.setImagemAnime(event.target.value)} placeholder="Imagem"/>

      <button type="submit" onClick={adicionarAnime}>Adicionar</button>
    </form>
}

export default Form;