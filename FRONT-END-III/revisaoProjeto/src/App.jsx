import { useState } from 'react'
import CardAnime from './CardAnime'
import Form from './Form'

function App() {
  const [nomeAnime, setNomeAnime] = useState("")
  const [imagemAnime, setImagemAnime] = useState("")
  const [animes, setAnimes] = useState([])
  const [text, setText] = useState("")

  return(

  <div>
    <h1>Animes</h1>
    <Form 
    // className={valor === "" ? styles.a : styles.b}
    text={text}
    setText={setText}
    nomeAnime={nomeAnime}
    imagemAnime={imagemAnime}
    animes={animes}
    setNomeAnime={setNomeAnime}
    setImagemAnime={setImagemAnime}
    setAnimes={setAnimes}
    />
    
    {animes.map((anime) => {
      return(
        <CardAnime anime={anime} />
      )
    })
  }
  </div>
  )

}

export default App
