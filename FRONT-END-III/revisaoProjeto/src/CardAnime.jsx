function CardAnime(props) {

  return(
    <div>
      <h1>{props.anime.nome}</h1>
      <img src={props.anime.url} />
    </div>
  )
}

export default CardAnime;