import {Link} from 'react-router-dom'

function App() {
  return (
    <div>
        <Navbar/>
        <Outlet />
        <Footer/>
    </div>

    // <div>
    //   <nav>
    //   {/*<Link/> nos permitirá mudar a URL sem “sairmos” realmente da URL da página onde estamos, alterando o conteúdo da tela.
    //     Cada Link indica o endereço para uma “rota”. O nome da rota será especificado dentro do atributo to. */}
    //     <Link to="/home">Home</Link>
    //     <Link to="/about">About</Link>
    //     <Link to="/faqs">About</Link>
    //   </nav>
    // </div>
  )
}

export default App
