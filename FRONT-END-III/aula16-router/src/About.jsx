import { useNavigate } from 'react-router-dom'
 
const About = () => {
  const navigate = useNavigate()
 
  return (
    <div>
         <button onClick={() => navigate(1)}>Go forward</button>
         <button onClick={() => navigate(-1)}>Go back</button>

    </div>
  )}

  export default About
