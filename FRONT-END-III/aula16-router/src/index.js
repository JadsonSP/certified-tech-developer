import { render } from 'react-dom'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import App from './App'
import Home from './Home'
import About from './About'
import Faqs from './Faqs'
import React from 'react'

ReactDOM.createRoot(document.getElementById('root')).render(


  <React.StrictMode>
  <BrowserRouter>
      <Routes>
         {/* Routes será o wrapper que envolva cada uma das nossas rotas, e Route será o componente que iremos usar para definir quando e o que iremos renderizar em cada URL.  */}
          <Route path='/' element={<App/>}  />
          {/* com path indicamos qual é a rota que deve matchear para exibir determinado componente ou “vista”. */}
          <Route path='/home' element={<Home/>}  />
          {/* O componente em si a ser roteado é especificado no atributo element. 
          Como este fragmento, indicamos agora ao React Router que, quando a URL matchear com /home, ele deve renderizar o elemento (ou componente) Home.
 */}
          <Route path='/about' element={<About/>}  />
          <Route path='faqs/:id' element={<Faqs/>}  />
      </Routes>

  </BrowserRouter>
  </React.StrictMode>
)