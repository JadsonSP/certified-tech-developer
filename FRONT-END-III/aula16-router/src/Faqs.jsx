import { Link, useParams } from "react-router-dom"

const Faqs = () => {
  const perguntas = [
    {
        id:1,
        title: "Eu posso fazer o curso sem ter experiência e/ou conhecimentos prévios?",
        descrição: "Sim. Na Digital House, você pode aprender do zero. Dependendo do curso no qual se inscreveu, vamos enviar para você o conteúdo prévio online."
    },
    {
	  id:2,
        title: "As vagas são limitadas?",
      descrição: "Sim. Elas têm uma capacidade máxima de entre 50 e 75 pessoas, dependendo do curso."
    },
   //...
]
const params = useParams()
  return (
    <div>
        <h1>Faqs</h1>
        <ol>
          {params.ir(pergunta => (
            <Link key={pergunta.id} to={`{pergunta.id}`}>

              <li>
              {pergunta.title}
              </li>
              
            </Link>
          ))}
        </ol>  
    </div> )
}
export default Faqs
