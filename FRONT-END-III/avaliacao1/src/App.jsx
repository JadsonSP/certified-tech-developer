import { useState } from 'react';
import Card from './Card';
import "./styles.css";

function App() {
  const [cards, setCards] = useState([]);
  const [valueInput, setValueInput] = useState({
    nome:"",
    lista:""
  });
  const [errorInput, setErrorInput] = useState("");
  //criar os Estados para manipular os Inputs
  function handleSubmit(event){
    if (valueInput.nome.length != 6 || valueInput.nome.length < 3) {

      setErrorInput("Digite uma cor valida"); 
      // (setValueInput([...cards, valueInput]))
      // setErrorInput("");
      // setValueInput("");

    } else {
      setCards([...cards,
        {nome: valueInput.nome,
        lista: valueInput.lista
        }])
        setErrorInput("");
        setValueInput({
          nome:"",
          lista:""
        });

    }

    event.preventDefault();
  }

  return (
    <div className="container">
      
      <form className="form"
       onSubmit={handleSubmit}>

      <h1>ADICIONAR NOVA COR</h1>

        <input className="container_input" 
        value={valueInput.nome} 
        onChange={(event) => setValueInput({...valueInput, nome: event.target.value})} placeholder='Digite o nome da cor'/>

        <input className="container_input2" value={valueInput.lista} 
        onChange={(event) => setValueInput({...valueInput, lista: event.target.value})} placeholder='sua cor é'/>

        <p>{errorInput}</p>
        <button type="submit">ADICIONAR</button>
      </form>

      {cards.map((cards)=> {
        return (

          <Card 
          // key={cards.nome}
          // nome={cards.nome} 
          // lista={cards.lista}
          />
        )
      })}
      

    </div>
  );
}

export default App
