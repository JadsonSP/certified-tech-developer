import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Dashboard from './pages/Dashboard'

const App = () =>{
  <BrowserRouter>
  <Routes>
    <Route path="/" element={<Home />}></Route>
    <Route path="Dashboard" element={<Dashboard />}></Route>
  </Routes>
  </BrowserRouter>
};

export default App;