import { Link } from "react-router-dom"

export default Navbar = () =>{
  return (
    <div>
      <Link to="/">Home</Link>
      <Link>Dashboard</Link>
    </div>
  )
}