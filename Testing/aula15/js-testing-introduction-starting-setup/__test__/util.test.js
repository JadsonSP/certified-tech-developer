const { generateText,validateInput, createElement } = require('../util.js');
/** @jest-environment jsdom */

describe('Teste da aplicação', () => {
    test('Saida de nome e idade', () => { 
        const text = generateText('Etevaldo', 690);
        expect(text).toBe('Etevaldo (690 years old)');
      });

    //   Validar saída de dados da aplicação

    test('Saída com dados vazios', () =>{
        const text = generateText('',null);
        expect(text).toBe(' (null years old)');    
    });
    
    test('Saída sem dados', () =>{
        const text = generateText();
        expect(text).toBe('undefined (undefined years old)');    
    });

      test('Validar as entradas das funções texto', () =>{
        const ret = validateInput('texto');
        expect(ret).toBeTruthy();
    });
    test('Validar as entradas das funções  empty', () =>{
        const ret = validateInput();
        expect(ret).toBeFalsy();
    });
    test('Validar as entradas das funções numéricas', () =>{
        const ret = validateInput(null,true,false);
        expect(ret).toBeFalsy();
    });
    test('Validar as entradas das funções texto empty', () =>{
        const ret = validateInput(" ",true,false);
        expect(ret).toBeFalsy();
    });
    test('Validar as entradas das funções texto NaN', () =>{
        const ret = validateInput(NaN,false,true);
        expect(ret).toBeFalsy();
    });
    

    // Validar, criar elementos
    test('Criar novo elemento data', () => {
        const element = createElement('li', "exampleElement", 'user-item');
        expect(element.textContent).toMatch(/exampleElement/);
    });
    test('Criar novo elemento sem data', () => {
        const element = createElement(null);
        expect(element.textContent).toBe("");
    });
})

