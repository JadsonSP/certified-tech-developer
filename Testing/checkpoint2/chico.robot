*** Settings ***
Library  SeleniumLibrary
Resource  ./checkpoint2.resource


*** Test Cases ***
1º Caso de Teste:Processo de compra de uma camiseta "Vira lata caramelo" no site do Chico Rei

  Clicar em "Concordar" no aviso sobre Cookies
  Clicar no campo procurar produto
  Pesquisar por "Vira lata caramelo" 
  Clicar no produto desejado
  Validar se o valor do produto é "R$ 69,90"
  Selecionar um tamanho
  Selecionar cor
  Clicar em "Ver sacola"
  Validar se a mensagem "VOCÊ ESTÁ ACUMULANDO 1 PONTO NO CARTÃO FIDELIDADE!"
  Clicar em "Fechar pedido"

Título do 2º Caso de Teste: Processo de compra de uma camiseta " " Amarelo é Desespero" 

  Clicar em "Concordar" no aviso sobre Cookies
  Clicar no campo procurar produto
  Pesquisar por Amarelo é Desespero" 
  Clicar no produto desejado
  Validar se o valor do produto é "R$ 69,90"
  Selecionar um tamanho
  Selecionar cor
  Clicar em "De segunda a sexta, das 8h às 20h."
  Clicar em "Fechar pedido"

3º Caso de Teste: Processo de compra de uma camiseta "Erarr é humano" no site do Chico Rei

 Clicar em "Concordar" no aviso sobre Cookies
  Clicar no campo procurar produto
  Pesquisar por "Erarr é humano" 
  Clicar no produto desejado
  Validar se o valor do produto é "R$ 69,90"
  Selecionar um tamanho
  Selecionar cor
  Clicar em "Ver sacola"
  Validar se a mensagem "De segunda a sexta, das 8h às 20h." FIDELIDADE!"
  Clicar em "Fechar pedido"

4º Caso de Teste: Processo de compra de uma camiseta "Nem Te Vi" no site do Chico Rei

 Clicar em "Concordar" no aviso sobre Cookies
  Clicar no campo procurar produto
  Pesquisar por "Nem Te Vi" 
  Clicar no produto desejado
  Validar se o valor do produto é "R$ 69,90"
  Selecionar um tamanho
  Selecionar cor
  Clicar em "Ver sacola"
  Validar se a mensagem "Compre parcelado com cartão ou á vista com boleto ou depósito"
  Clicar em "Fechar pedido"

5º Caso de Teste: Processo de compra de uma camiseta "Alceu Dispor" no site do Chico Rei

Clicar em "Concordar" no aviso sobre Cookies
  Clicar no campo procurar produto
  Pesquisar por  "Alceu Dispor" 
  Clicar no produto desejado
  Validar se o valor do produto é "R$ 69,90"
  Selecionar um tamanho
  Selecionar cor
  Clicar em Validar se a mensagem "Sua primeira troca na Chico Rei é por nossa conta!" está sendo exibida

  Clicar em "Fechar pedido"



