import { useReducer } from "react";

let estadoInicial = {contador : 0};

let funcaoRedutora = (state, action) => {
  switch (action.tipo) {

    case "aumentar":
      return { contador: state.contador + 1};

    case "diminuir":
      return { contador: state.contador - 1 };

    case "reiniciar":
    return { contador: 0 };
  
    default:
      throw new Error("Nenhuma ação recebida...")
  }
};

function Contador() {
  const [estado, infoEntrega] = useReducer(funcaoRedutora, estadoInicial);


return (
  <>
    <h1>A contagem é ...</h1>
    <div>
      <button 
      onClick={() =>{
        infoEntrega({tipo: "diminuir"});
      }}>Diminuir</button>
      
      <button
      onClick={() =>{
        infoEntrega({tipo:"aumentar"})
      }}>Aumentar</button>

      <button 
      onClick={() =>
        infoEntrega({tipo: "reiniciar"})}>Reiniciar</button>
    </div>
  </>
);
}



// export default (props) => {
  // let [contador, setContador] = useState(0);
  // let handleIncrementClick = () => {
  //   setContador((prevState) => prevState + 1);
  // };


  // let handleDecrementClick = () => {
  //   setContador((prevState) => prevState - 1);
  // };


  // let handleResetClick = () => {
  //   setContador(0);
  // };


  // return (
    <>
      {/* <h1>A contagem é {contador}</h1>
      <div>
        <button
          onClick={() => {
            handleDecrementClick();
          }}
        >
          Diminuir
        </button>
        <button
          onClick={() => {
            handleIncrementClick();
          }}
        >
          Aumentar
        </button>
        <button onClick={() => handleResetClick()}>Reiniciar</button>
      </div> */}
    </>
 

