import styles from './card.module.css';

const Cards = (props) => {
  function tratarPreco(price){

        
    if(price.toString().length>3){
        
        const stringPrice = price.toString()
        return 'R$ '+stringPrice[0]+'.'+stringPrice[1]+stringPrice[2]+stringPrice[3]+',00'
        
    }

    return 'R$ '+price+',00'
}
  return (
    <div className={styles.card}>
            <img className={styles.img} src={props.value.images[0]}/>
            <div className={styles.content}>
                <p className={styles.titleDescription}>{props.value.title} {props.value.description}</p>
                <p className={styles.category}>{props.value.category.toUpperCase()}</p>
                <p className={styles.price}>{tratarPreco(props.value.price)}</p>
            </div>
        </div>
  )
}

export default Cards
