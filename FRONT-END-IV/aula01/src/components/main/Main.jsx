import { useState, useEffect } from "react"
import axios from "axios";
import styles from "./main.module.css"
import Cards from "../cards/Cards";

const Main = () => {
  const [produtos, setProdutos] = useState([])
  
  useEffect(()=>{
    getProdutos()
  },[])

  async function getProdutos(){
    const response = await axios.get('https://dummyjson.com/products')

    console.log(response.data.products)

    setProdutos(response.data.products)

  }

  return (
    <main className={styles.main}>

        {produtos.map((item)=>(
          <Cards key={item.id} value={item}/>
          
          ))}
      
    </main>

  )
}

export default Main
