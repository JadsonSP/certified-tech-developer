import styles from "./header.module.css"

const Header = () => {
  return (
    <div className={styles.header}>
      <p>Shop Products</p>
    </div>
  )
}

export default Header
