package com.example.MongoDB.controller;

import com.example.MongoDB.model.TorneioModel;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/torneio")
public class TorneioController {
    private TorneioService torneioService;

    public TorneioController(TorneioService torneioService){
        this.torneioService = torneioService;
    }

    @PostMapping
    public TorneioModel adicionarTorneio(@RequestBody TorneioModel torneio){
        return torneioService.guardarTorneio(torneio);

    }

    @GetMapping
    public List<TorneioModel> listarTorneios(){
        return torneioService.listarTorneios();
    }
}
