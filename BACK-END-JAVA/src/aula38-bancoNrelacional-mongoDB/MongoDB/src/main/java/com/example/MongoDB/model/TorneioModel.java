package com.example.MongoDB.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "torneio")
public class TorneioModel {

    @Id
    private Integer id;
    private String nome;
    private String pais;

}
