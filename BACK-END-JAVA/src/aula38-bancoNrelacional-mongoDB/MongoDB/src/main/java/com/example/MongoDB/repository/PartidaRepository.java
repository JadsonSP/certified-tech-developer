package com.example.MongoDB.repository;

import com.example.MongoDB.model.PartidaModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public class PartidaRepository extends MongoRepository<PartidaModel, Integer> {

    List<PartidaModel> findAllByEstado (EstadoModel estado);
}
