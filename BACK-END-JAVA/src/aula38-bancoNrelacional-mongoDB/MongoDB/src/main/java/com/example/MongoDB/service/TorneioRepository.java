package com.example.MongoDB.service;

import com.example.MongoDB.model.TorneioModel;

import java.util.List;

public class TorneioRepository {
    private TorneioRepository torneioRepository;

    public TorneioService(TorneioRepository torneioRepository){
        this.torneioRepository = torneioRepository;
    }

    public TorneioModel guardarTorneio(TorneioModel torneio) {
        return new TorneioRepository.save(torneio);
    }

    public List<TorneioModel> listarTorneios(){
        return torneioRepository.findAll();
    }

}
