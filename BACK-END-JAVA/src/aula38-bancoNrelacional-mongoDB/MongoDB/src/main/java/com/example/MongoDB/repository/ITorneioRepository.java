package com.example.MongoDB.repository;

import com.example.MongoDB.model.TorneioModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ITorneioRepository extends MongoRepository<TorneioModel, Integer> {
}
