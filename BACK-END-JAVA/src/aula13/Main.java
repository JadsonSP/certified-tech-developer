import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class Main {

    private static final String sqlCreate = "DROP TABLE IF EXISTS Dentista;" +
            "CREATE TABLE Dentista" +
            "(" +
            "id INT PRIMARY KEY," +
            "name VARCHAR(100) NOT NULL," +
            "sobrenome VARCHAR(100) NOT NULL," +
            "matricula VARCHAR(100) NOT NULL" +
            ");";

    private  static final String sqlInsert = "INSERT INTO Dentista(id, name,sobrenome, matricula) VALUES (?,?,?,?);";
    public static final String sqlUpdate = "UPDATE Dentista SET matricula = ? WHERE id = ?;";
    public  static void main(String[] args) throws Exception {
        Dentista dentista = new Dentista(12,"Matheus", "Sílva", "545124545");

        Connection connection = null;

        try{
            connection = Connection.getConnection();
            Statement statement = connection.createStatement();
            statement.execute(sqlCreate);

            PreparedStatement preparedStatementInsert = connection.prepareStatement();
        };

    }

}