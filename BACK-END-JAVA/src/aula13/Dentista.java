import java.util.Collections;

public class Dentista {
    private String name;

    private String sobrenome;
    private String matricula;
     private int id;

    public Dentista( int id,String name, String sobrenome, String matricula) {
        this.id = id;
        this.name = name;
        this.sobrenome = sobrenome;
        this.matricula = matricula;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
