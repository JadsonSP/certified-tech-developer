package com.aulasdepoo.integradoraaula18.exercicioguiado1;


import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Selecao selecao = new Selecao("Seleção Brasileira");

        Jogador jogador1 = new Jogador(1, "Spatz", "Goleiro");
        Jogador jogador2 = new Jogador(2, "Lauch", "Meio Campista");
        Jogador jogador3 = new Jogador(3, "Covalski", "Meio Campista");
        Jogador jogador4 = new Jogador(4, "Silva", "Defensor");
        Jogador jogador5 = new Jogador(5, "Santos", "Reserva");
        Jogador jogador6 = new Jogador(6, "Oliveira", "Suplente");
        Jogador jogador7 = new Jogador(7, "Souza", "Defensor");
        Jogador jogador8 = new Jogador(8, "Rodrigues", "Defensor");
        Jogador jogador9 = new Jogador(9, "Ferreira", "Goleiro");
        Jogador jogador10 = new Jogador(10, "Tolotti", "Reserva");
        Jogador jogador11 = new Jogador(11, "Pereira", "Atacante");
        Jogador jogador12 = new Jogador(12, "Ribeiro", "Suplente");
        Jogador jogador13 = new Jogador(13, "Martins", "Defensor");
        Jogador jogador14 = new Jogador(14, "Almeida", "Atacante");
        Jogador jogador15 = new Jogador(15, "Andrade", "Reserva");
        Jogador jogador16 = new Jogador(16, "Barbosa", "Goleiro");
        Jogador jogador17 = new Jogador(17, "Barros", "Defensor");
        Jogador jogador18 = new Jogador(18, "Batista", "Goleiro");
        Jogador jogador19 = new Jogador(19, "Borges", "Atacante");
        Jogador jogador20 = new Jogador(20, "Dias", "Goleiro");
        Jogador jogador21 = new Jogador(21, "Lopes", "Defensor");
        Jogador jogador22 = new Jogador(22, "Medeiros", "Reserva");
        Jogador jogador23 = new Jogador(23, "Cardoso", "Reserva");

        selecao.addJogador(jogador1);
        selecao.addJogador(jogador2);
        selecao.addJogador(jogador3);
        selecao.addJogador(jogador4);
        selecao.addJogador(jogador5);
        selecao.addJogador(jogador6);
        selecao.addJogador(jogador7);
        selecao.addJogador(jogador8);
        selecao.addJogador(jogador9);
        selecao.addJogador(jogador10);
        selecao.addJogador(jogador11);
        selecao.addJogador(jogador12);
        selecao.addJogador(jogador13);
        selecao.addJogador(jogador14);
        selecao.addJogador(jogador15);
        selecao.addJogador(jogador16);
        selecao.addJogador(jogador17);
        selecao.addJogador(jogador18);
        selecao.addJogador(jogador19);
        selecao.addJogador(jogador20);
        selecao.addJogador(jogador21);
        selecao.addJogador(jogador22);
        selecao.addJogador(jogador23);


        selecao.obterReservas(selecao.jogadoresSelecao);

        try {
            selecao.obterQuantidadeJogadores("suplentes ");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
