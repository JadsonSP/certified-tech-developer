package com.aulasdepoo.integradoraaula18.exercicioguiado1;


import java.util.ArrayList;
import java.util.Collections;

public class Selecao {
    private String nome;
    ArrayList<Jogador> jogadoresSelecao = new ArrayList<>();

    public Selecao(String nome) {
        this.nome = nome;
    }

    public void addJogador(Jogador jogador){
        jogadoresSelecao.add(jogador);
    }

    public void obterReservas(ArrayList<Jogador> jogadores){
        int i = 18;
        while(i < jogadores.size()){ //pega os 11 primeiros da lista pois inicia em 0
            jogadoresSelecao.get(i);
            System.out.println("Jogador " + i + " Sobrenome: " +  jogadoresSelecao.get(i).getSobrenome());
            i++;
        }
    }

    public void obterQuantidadeJogadores(String posicao) throws JogadorException {
        int contador = 0;
        for (Jogador jogador: jogadoresSelecao) {
            if(posicao == "Goleiro" || posicao == "Defensor" || posicao == "Meio Campista" || posicao == "Atacante") {
                if (jogador.getPosicao() == posicao) {
                    contador++;
                }
            }else{
                throw new JogadorException("A posição não exite");
            }
        }
        System.out.println("Quantidade de jogadores da posição informada: " + contador);
    }
}

