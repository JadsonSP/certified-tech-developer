package com.aulasdepoo.integradoraaula18.exercicioguiado1;

public class Jogador {

    private int numeroCamisa;
    private String sobrenome;
    private String posicao;

    public Jogador(int numeroCamisa, String sobrenome, String posicao) {
        this.numeroCamisa = numeroCamisa;
        this.sobrenome = sobrenome;
        this.posicao = posicao;
    }

    public int getNumeroCamisa() {
        return numeroCamisa;
    }

    public void setNumeroCamisa(int numeroCamisa) {
        this.numeroCamisa = numeroCamisa;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }
}
