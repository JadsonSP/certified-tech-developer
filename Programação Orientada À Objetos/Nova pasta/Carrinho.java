package composite;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Carrinho {

    private ArrayList<Item> listaItensTotaisDoCarrinho = new ArrayList<>();

    public void addItem (Item item){
        listaItensTotaisDoCarrinho.add(item);
    }

    public void mostrarItem(){
        for(Item item: listaItensTotaisDoCarrinho){
            System.out.println(item.getNome());
        }
    }

    public double valorTotalCarrinho(){
        double valor = 0;
        for (Item item: listaItensTotaisDoCarrinho){
            valor += item.calcularPreco();
        }
        return  valor;
    }
}
