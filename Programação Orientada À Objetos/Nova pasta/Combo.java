package composite;

import java.util.ArrayList;

public class Combo extends Item{

    private ArrayList<Item> listaItens = new ArrayList<>();

    public Combo(String nome) {
        super(nome);
    }

    public void inserirItens(Item item){
        listaItens.add(item);
    }



    @Override
    public double calcularPreco() {
        double valorCombo = 0;
        for (Item item: listaItens){
            valorCombo += item.calcularPreco();
        }
        return  valorCombo;
    }
}
