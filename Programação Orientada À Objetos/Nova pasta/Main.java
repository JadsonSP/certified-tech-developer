package composite;

public class Main {

    public static void main(String[] args) {
        Carrinho c = new Carrinho();

        Item item1 = new Produto("Hamburguer", 10.5);
        Item item2 = new Produto("Batata Frita", 6.0);
        Item item3 = new Produto("Refri", 13.9);

        Item combo = new Combo("Mac Lanche Feliz");



        ((Combo)combo).inserirItens(item1);
        ((Combo)combo).inserirItens(item2);
        ((Combo)combo).inserirItens(item3);

        c.addItem(combo);
        c.mostrarItem();
        System.out.println("Valor total da compra R$: " +
                c.valorTotalCarrinho());





    }
}
