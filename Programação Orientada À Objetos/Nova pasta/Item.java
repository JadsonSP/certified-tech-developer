package composite;

public abstract class Item {

    public String nome;

    public Item(String nome) {
        this.nome = nome;
    }

    public abstract double calcularPreco();


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
