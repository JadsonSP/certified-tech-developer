package aula11;

public abstract class Animal {
    private String nome;
    private int idade;

    public abstract void emitirSom();

    public abstract void movimentar();

    public Animal(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;

        System.out.println("Olá, meu nome é "+this.getNome()+", eu sou um(a) "+ this.getClass().getSimpleName() +" e tenho "+this.getIdade()+" anos");
        System.out.println("-----------------------------------------------------");
    }

    public int getIdade() {
        return idade;
    }

    public String getNome() {
        return nome;
    }
}
