package aula11;

public class Cavalo extends Animal{
    public Cavalo(String nome, int idade) {
        super(nome, idade);
    }

    @Override
    public void emitirSom() {
        System.out.println("Hiin in in!");
    }

    @Override
    public void movimentar() {
        System.out.println("O cavalo está cavalgando");
    }

    public void correr(){
        System.out.println("O cavalo está correndo");
    }

    public void correr(int intensidade){
        if(intensidade==0){
            System.out.println("O cavalo está cavalgando devagar");
        } else if (intensidade==1) {
            System.out.println("O cavalo está cavalgando bem");
        }
        else{
            System.out.println("O cavalo despirocou");
        }
    }
}
