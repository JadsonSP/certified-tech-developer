package aula11;

public class Main {
    public static void main(String[] args) {
        Cachorro cachorro1 = new Cachorro("dog", 10, "Vira-lata");
        Cavalo cavalo1 = new Cavalo("Pé de Pano", 20);
        Preguica preguica1 = new Preguica("Preguiçola", 25);

        System.out.println("-----------------------------------------------------");

        cachorro1.correr();
        cachorro1.correr(1);
        cachorro1.correr(2);
        cachorro1.movimentar();
        cachorro1.emitirSom();

        System.out.println("-----------------------------------------------------");

        cavalo1.correr();
        cavalo1.correr(1);
        cavalo1.correr(2);
        cavalo1.movimentar();
        cavalo1.emitirSom();

        System.out.println("-----------------------------------------------------");

        preguica1.subirEmArvore();
        preguica1.movimentar();
        preguica1.emitirSom();
    }
}
