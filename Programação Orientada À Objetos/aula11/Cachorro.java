package aula11;

public class Cachorro extends Animal{
    private String raca;
    public Cachorro(String nome, int idade, String raca) {
        super(nome, idade);
        this.raca = raca;
    }

    @Override
    public void emitirSom() {
        System.out.println("Woof Woof!");
    }

    @Override
    public void movimentar() {
        System.out.println("O cachorro está andando");
    }

    public void correr(){
        System.out.println("O cachorro está correndo");
    }

    public void correr(int intensidade){
        if(intensidade==0){
            System.out.println("O cachorro está andando devagar");
        } else if (intensidade==1) {
            System.out.println("O cachorro está andando bem");
        }
        else{
            System.out.println("O cachorro despirocou");
        }
    }

    public String getRaca() {
        return raca;
    }
}
