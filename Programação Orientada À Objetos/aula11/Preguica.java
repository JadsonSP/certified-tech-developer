package aula11;

public class Preguica extends Animal{

    public Preguica(String nome, int idade) {
        super(nome, idade);
    }

    @Override
    public void emitirSom() {
        System.out.println("AAAaaaAAAaa ... AAaaaAAAaaAAAaAAAaa!");
    }

    @Override
    public void movimentar() {
        System.out.println("O preguiça está se movimentando, na sua velocidade");
    }

    public void subirEmArvore(){
        System.out.println("A preguiça está subindo em uma árvore.");
    }
}
