package aula12;

public abstract class Assistente extends Funcionario{
    private int matricula;

    public Assistente(String nome, Double salario, int matricula) {
        super(nome, salario);
        this.matricula = matricula;
    }

    @Override
    public void exibeDados() {
        System.out.println("Nome: "+this.getNome());
        System.out.println("Salario Base: "+this.getSalario());
        System.out.println("Matricula: "+this.getMatricula());

        if(this.getClass().getSimpleName().equals("Administrativo")){
            boolean turnoNoturno = ((Administrativo) this).getTurnoNoturno();
            if (turnoNoturno){
                System.out.println("Turno Noturno? Sim");
                System.out.println("Adicional Noturno: "+((Administrativo)this).getAdicionalNoturno());
                System.out.println("Salário Total: "+((Administrativo)this).getNovoSalario());
                System.out.println("Salário Anual: "+this.ganhoAnual());
            }
            else{
                System.out.println("Turno Noturno? Não");
            }
        }
        else{
            System.out.println("Bonus Salarial: "+((Tecnico)this).getBonusSalarial());
            System.out.println("Salário Total: "+((Tecnico)this).getNovoSalario());
            System.out.println("Salário Anual: "+this.ganhoAnual());
        }
    }

    public int getMatricula() {
        return matricula;
    }
}
