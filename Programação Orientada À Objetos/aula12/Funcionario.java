package aula12;

public abstract class Funcionario {
    private String nome;
    private Double salario;

    public Funcionario(String nome, Double salario) {
        this.nome = nome;
        this.salario = salario;
    }

    public void addAumento(double valor){
        this.salario += valor;
    }

    public Double ganhoAnual(){
        return this.salario * 12.0;
    }

    public abstract void exibeDados();

    public String getNome() {
        return this.nome;
    }

    public Double getSalario() {
        return this.salario;
    }
}
