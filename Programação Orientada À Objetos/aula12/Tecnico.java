package aula12;

public class Tecnico extends Assistente{
    private Double bonusSalarial;

    public Tecnico(String nome, Double salario, int matricula, double bonusSalarial) {
        super(nome, salario, matricula);
        this.bonusSalarial = bonusSalarial;
    }

    @Override
    public Double ganhoAnual() {
        return super.ganhoAnual()+(this.bonusSalarial*12);
    }
    public Double getNovoSalario() {
        return super.getSalario()+this.bonusSalarial;
    }
    public Double getBonusSalarial() {
        return bonusSalarial;
    }
}
