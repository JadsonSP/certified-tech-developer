package aula12;

public class Administrativo extends Assistente{
    private Double adicionalNoturno;
    private boolean turnoNoturno;

    public Administrativo(String nome, Double salario, int matricula, double adicionalNoturno, boolean turnoNoturno) {
        super(nome, salario, matricula);
        this.adicionalNoturno = adicionalNoturno;
        this.turnoNoturno = turnoNoturno;
    }
    @Override
    public Double ganhoAnual() {
        return super.ganhoAnual()+(this.adicionalNoturno*12);
    }

    public Double getAdicionalNoturno() {
        return adicionalNoturno;
    }

    public Double getNovoSalario() {
        return super.getSalario()+adicionalNoturno;
    }

    public boolean getTurnoNoturno(){
        return this.turnoNoturno;
    }
}
