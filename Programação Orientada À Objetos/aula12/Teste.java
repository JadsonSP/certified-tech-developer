package aula12;

public class Teste {
    public static void main(String[] args) {
        Assistente assistente1 = new Tecnico("Joaquim", 2500.00, 123456, 500.00);
        Assistente assistente2 = new Administrativo("José", 2200.00, 123456, 800.00, true);

        assistente1.exibeDados();

        System.out.println("--------------------------");

        assistente2.exibeDados();
    }
}
