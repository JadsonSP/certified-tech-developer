package aula20state;

public interface EstadoCarro {

    void acelerar();

    void freiar();

    void ligar();


}