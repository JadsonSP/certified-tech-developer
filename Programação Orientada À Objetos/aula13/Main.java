package com.aulasdepoo.aula13.exercicioguiado;

public class Main {
    public static void main(String[] args) {
        ContaCorrente cc = new ContaCorrente(5698.00,9000.00);
        cc.sacar(587.56);
        cc.imposto(3.0);

        ContaPoupanca cp = new ContaPoupanca(6250.75);
        cp.sacar(5.0);
        cp.depositar(47.41);
    }
}
