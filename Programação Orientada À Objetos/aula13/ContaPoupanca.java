package com.aulasdepoo.aula13.exercicioguiado;

public class ContaPoupanca extends Conta{

    public ContaPoupanca(Double saldo) {
        super(saldo);
    }

    @Override
    public void sacar(Double valor) {
        if(valor <= getSaldo()){
            System.out.println("Saque de R$" + valor + " efetuado com sucesso!");
        }

    }

    public void cobrarJuros(){
        System.out.println("Juros são de 5%, valor R$" + getSaldo() * 0.5);
    }
}
