package com.aulasdepoo.aula13.exercicioguiado;

public class ContaCorrente extends Conta implements GravarImposto{

    private Double valorPermitido;

    public ContaCorrente(Double saldo, Double valorPermitido) {
        super(saldo);
        this.valorPermitido = valorPermitido;
    }

    //sobrescrita da classe pai
    @Override
    public void sacar(Double valor) {
        if(valor <= valorPermitido){
            System.out.println("Saque de R$" + valor + " efetuado com sucesso!");
        }
    }

    //implementação da interface
    @Override
    public void imposto(Double porcentagem) {
        double porc = porcentagem/100;
        System.out.println("Saldo com desconto de imposto é de R$" + (getSaldo()-(getSaldo()*porc)) + ". Desconto de R$" + getSaldo()*porc);
    }

    public Double getValorPermitido() {
        return valorPermitido;
    }

    public void setValorPermitido(Double valorPermitido) {
        this.valorPermitido = valorPermitido;
    }

}
