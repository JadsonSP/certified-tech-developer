package com.aulasdepoo.aula13.exercicioguiado;

public interface GravarImposto {

    public void imposto(Double porcentagem);
}
