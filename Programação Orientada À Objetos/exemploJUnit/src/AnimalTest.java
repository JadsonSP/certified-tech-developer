import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnimalTest {
    @Test
    public void SeAnimalPesado(){
        Animal cavalo = new Animal("Cavalo","grande", 750);
        Animal cachorro = new Animal("Cachorro","Pequeno",8);

        boolean ePesado = cachorro.EPesado();

        assertEquals(true, cavalo.EPesado());
        assertFalse(ePesado);
    }

}