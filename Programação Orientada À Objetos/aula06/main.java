import adocao.ADOCAO;

import java.time.LocalDate;

public class main {

    public static void main(String[] args) {

        ADOCAO cachorro1 = new ADOCAO("SRD", LocalDate.of(2018, 8, 10),
                5.0, true, false,"Caramelo");

        System.out.println(cachorro1.getIdade());

        System.out.println(cachorro1.isPossuiChip());

        cachorro1.caminharParaAdocao();
    }
}