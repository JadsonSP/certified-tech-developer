import java.time.LocalDate;

public class ADOCAO {

    private String raca;
    private LocalDate anoNascimento;
    private Double peso;
    private boolean possuiChip;
    private boolean machucado;
    private boolean aptoAdocao;
    private String nome;


    public ADOCAO(String raca, LocalDate anoNascimento, Double peso, boolean possuiChip, boolean machucado, String nome) {
        this.raca = raca;
        this.anoNascimento = anoNascimento;
        this.peso = peso;
        this.possuiChip = possuiChip;
        this.machucado = machucado;
        this.nome = nome;
    }

    public int getIdade(){
        LocalDate dataAtual = LocalDate.now();

        LocalDate idade =  dataAtual.minusYears(getAnoNascimento().getYear());

        return idade.getYear();
    }

    public void caminharParaAdocao(){

        Double peso = getPeso();

        boolean estaFerido = isMachucado();

        if(peso >= 5 && estaFerido != true){
            this.aptoAdocao = true;
            System.out.println("Liberado para adoção: " + this.aptoAdocao);
        }else {
            System.out.println("Liberado para adoção: " + this.aptoAdocao);
        }
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public LocalDate getAnoNascimento() {
        return anoNascimento;
    }

    public void setAnoNascimento(LocalDate anoNascimento) {
        this.anoNascimento = anoNascimento;
    }

    public boolean isPossuiChip() {
        return possuiChip;
    }

    public void setPossuiChip(boolean possuiChip) {
        this.possuiChip = possuiChip;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public boolean isMachucado() {
        return machucado;
    }

    public void setMachucado(boolean machucado) {
        this.machucado = machucado;
    }

    public boolean isAptoAdocao() {
        return aptoAdocao;
    }

    public void setAptoAdocao(boolean aptoAdocao) {
        this.aptoAdocao = aptoAdocao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
