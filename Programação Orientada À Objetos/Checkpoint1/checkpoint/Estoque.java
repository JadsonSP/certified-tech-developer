package checkpoint;

import java.util.Date;

public class Estoque {
    private String medicamentos;
    public static int quantidade;
    private Date dataValidade;

    public Estoque(String medicamentos, Date dataValidade, int quantidade) {
        this.medicamentos = medicamentos;
        this.dataValidade = dataValidade;
        this.quantidade = quantidade;
    }

    public String getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(String medicamentos) {
        this.medicamentos = medicamentos;
    }

    public Date getDataValidade() {
        return dataValidade;
    }

    public void setDataValidade(Date dataValidade) {
        this.dataValidade = dataValidade;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
