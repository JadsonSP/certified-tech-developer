package checkpoint;

public class Medicamento {
    public static String nome;
    protected static int quantidade;
    private boolean receitaUsuario;

    public Medicamento(String nome, int quantidade, boolean receitaUsuario) {
        this.nome = nome;
        this.quantidade = quantidade;
        this.receitaUsuario = true;
    }

    protected static int quantidade() {
        return 0;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public boolean isReceitaUsuario() {
        return receitaUsuario;
    }

    public void setReceitaUsuario(boolean receitaUsuario) {
        this.receitaUsuario = receitaUsuario;
    }
}
