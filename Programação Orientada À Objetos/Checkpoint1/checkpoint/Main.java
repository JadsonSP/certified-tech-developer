package checkpoint;

import static checkpoint.Medicamento.nome;

public class Main {

    public static void main(String[] args){
        Drogaria drogaria1 = Drogaria ("Drogaria um Passo para o Inferno", "Rua:Dos Zé ninguém", 9999, 66666666);
        Estoque estoque1 = Estoque( "medicamentos: Todos", LocalDate.of(2010,05,20), 100);
        Farmaceutico farmaceutico1 = Farmaceutico("Capiroto", 666);
        Medicamento medicamento1 = Medicamento("Rivotril", 50, "Sim");
        Cliente cliente1 = Cliente ("Zé Droguinha", 66666666666, "Rua dos Loucos", "9999999", "kaciudis_morri@hell.com");
        Compra compra1 = Compra("Rivotril", 50, "true", LocalDate.of(2059,05,20), "Debito", 7.50,  375);

        farmaceutico1.verificarReceita();
        farmaceutico1.consultarEstoque();
        cliente1.consultarMedicamento();
        cliente1.comprarMedicamento();
        compra1.calcularCompra();
        drogaria1.enviarMedicamento();

    }
}
