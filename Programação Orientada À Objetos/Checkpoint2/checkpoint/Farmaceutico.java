package checkpoint;

public class Farmaceutico {
    private String nome;
    private int crm;

    public Farmaceutico(String nome, int crm) {
        this.nome = nome;
        this.crm = crm;
    }

    public void verificarReceita(){
        boolean medicamento = true;
        if(medicamento ==  true  ){
            System.out.println("Medicamento precisa de receita, solicitar o upload");
        }else {
            System.out.println("Medicamento pode ser liberado sem receita");
        }
    }

    public void consultarEstoque(){

        if(Estoque.quantidade >= 1){
            System.out.println("Existe" + Estoque.quantidade + "do" + Medicamento.nome + "estoque" );
        }else {
            System.out.println("Medicamento em falta");
        }
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCrm() {
        return crm;
    }

    public void setCrm(int crm) {
        this.crm = crm;
    }
}
