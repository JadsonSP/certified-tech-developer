package checkpoint;

import java.time.LocalDate;

public class Compra extends Medicamento {
    private LocalDate data;
    private String formaPagamento;
    private Double valorItem;
    private Double valorTotal;


    public Compra(String nome, int quantidade, boolean receitaUsuario, LocalDate data, String formaPagamento, Double valorItem, Double valorTotal) {
        super(nome, quantidade, receitaUsuario);
        this.data = data;
        this.formaPagamento = formaPagamento;
        this.valorItem = valorItem;
        this.valorTotal = valorTotal;
    }

    public void calcularCompra() {
        this.valorTotal = this.valorItem * Medicamento.quantidade();

        System.out.println("O valor Total é: " + this.valorTotal);
    }


    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public Double getValorItem() {
        return valorItem;
    }

    public void setValorItem(Double valorItem) {
        this.valorItem = valorItem;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }
}
