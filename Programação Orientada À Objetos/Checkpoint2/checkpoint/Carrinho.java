package checkpoint;
import java.util.ArrayList;

public class Carrinho {

  private ArrayList<Produto>itensTotalCarrinho = new ArrayList<>();

  public void add(Produto produto) {
    itensTotalCarrinho.add(produto);
  }

  public void mostrarProduto() {
    for(Produto produto : itensTotalCarrinho) {
      System.out.println((produto.getName()));
    }
  }

  public double totalCarrinho() {
    double valor = 0;
    for (Produto produto : itensTotalCarrinho) {
      valor += produto.calcularCompra();
    }
    return valor;
  }
}
