package checkpoint;

import java.util.ArrayList;

public class Promocao extends Produto {

  private ArrayList<Produto> listaProdutos = new ArrayList<>();

  public Promocao(String name) {
    super(nome);
  }

  public void inserirProdutos(Produto produto) {
    listaProdutos.add(produto);
  }

  @Override
  public double calcularCompra() {
    double valorPromocao = 0;
    for (Produto produto : listaProdutos) {
      valorPromocao += produto.calcularCompra();
    }
    return valorPromocao;
  }

  @Override
  public double calcularPreco() {
    // TODO Auto-generated method stub
    return 0;
  }
  
}
