package checkpoint;

public class Medicamento extends Produto {
    
    private double preco;

    public Medicamento(String nome, double preco) {
        super(nome);
        this.preco = preco;
    }

    @Override
    public double calcularCompra() {
        return this.preco;
    }

    @Override
    public double calcularPreco() {
        // TODO Auto-generated method stub
        return 0;
    }

   
}
