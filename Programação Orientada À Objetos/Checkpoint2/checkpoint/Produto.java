package checkpoint;

public abstract class Produto {

  public static String nome;

  public Produto(String nome) {
      this.nome = nome;
  }

  public abstract double calcularPreco();


  public String getNome() {
      return nome;
  }

  public void setNome(String nome) {
      this.nome = nome;
  }

  public double calcularCompra() {
    return 0;
  }

  public char[] getName() {
    return null;
  }

  public void inserirProdutos(Produto item1) {
  }
}
