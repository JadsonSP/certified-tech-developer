package com.aulasdepoo.aula19.exercicioguiado;

import java.util.ArrayList;

public class Empresa {
    private String cnpj;
    private ArrayList<Empregado> listaEmpregado = new ArrayList<>();

    public Empresa(String cnpj) {
        this.cnpj = cnpj;
    }

    public void calcularSalarioTotal(int dias){
        for (Empregado empregado : listaEmpregado) {
            System.out.println("O empregado(a) " + empregado.getNome() +
                    " possui salário é de R$" + empregado.calcularSalario(dias));
        }
    }

    public void addEmpregado(Empregado empregado){
        listaEmpregado.add(empregado);
    }

    public void lerEmpregado(){
        for (Empregado e: listaEmpregado){
            System.out.println("Empregado: " + e.getNome());
        }

    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public ArrayList<Empregado> getListaEmpregado() {
        return listaEmpregado;
    }

    public void setListaEmpregado(ArrayList<Empregado> listaEmpregado) {
        this.listaEmpregado = listaEmpregado;
    }
}
