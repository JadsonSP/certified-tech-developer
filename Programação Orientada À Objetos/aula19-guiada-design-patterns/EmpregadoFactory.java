package com.aulasdepoo.aula19.exercicioguiado;

public class EmpregadoFactory {

    //Padrão Singleton: é um design patter criacional que garante que essa classe só tenha UMA instancia de define um ponto de acesso global a ela.
    //quando uma classe não vai ser instanciada fora dela mesma (nem subclasses instanciam) e mais que uma vez. Ex: classe de conexão com banco de dados.

    //Padrão Singleton: Criar um atributo estático do mesmo tipo da classe com o nome instance;
    private static EmpregadoFactory instance;

    //Padrão Singleton: Todos os construtores da classe devem utilizar o modificador private;
    private EmpregadoFactory(){}

    //Padrão Singleton: Criar um método estático getInstance() que retorna o atributo instance.
    public static EmpregadoFactory getInstance(){
        if (instance == null){
            instance = new EmpregadoFactory();
        }
        return instance;
    }

    //Padrão Factory: um dos principais e mais utilizados.  Padrão que fornece uma interface para criar família de objetos dependetes ou
    // relacionados sem especificar suas classes concretas
    public Empregado criarEmpregado(String tipo, String nome, double salario){
        if (tipo.equals("EMP-INT")){
            EmpregadoRelacaoDep empregadoRelacaoDep = new  EmpregadoRelacaoDep();
            empregadoRelacaoDep.setNome(nome);
            empregadoRelacaoDep.setSalarioMensal(salario);
            return empregadoRelacaoDep;
        }else  if(tipo.equals("EMP-EXT")){
            EmpregadoContratado empregadoContratado = new  EmpregadoContratado();
            empregadoContratado.setNome(nome);
            empregadoContratado.setValorPorHora(10.0);
            empregadoContratado.setImposto(5.0);
            return empregadoContratado;
        }
        return null;
    }
}
