package com.aulasdepoo.aula19.exercicioguiado;

public class Main {
    public static void main(String[] args) {
        Empresa empresa = new Empresa("12345");
        EmpregadoFactory empregado = EmpregadoFactory.getInstance();

        //Já um hashcode(código hash) é um valor inteiro associado com todos os objetos
        // em Java. Então, para obter esse hashcode precisamos utilizar o método
        // hashCode(), esse método retornará um inteiro para o objeto passado
      //  System.out.println(empregado.hashCode());

        empresa.addEmpregado(empregado.criarEmpregado("EMP-INT",  "Junior", 5878.56));
        empresa.addEmpregado(empregado.criarEmpregado("EMP-EXT", "Aurea",  4780.00));

        empresa.lerEmpregado();
        empresa.calcularSalarioTotal(25);
    }
}
